# -*- coding: utf-8 -*-
"""Edge Module"""
from .point import Point
from .segment import Segment


class Edge(Segment):
    """
    **Input:**
    
    - Edge(Point, Point, label='unknown')
    """
    def __init__(self, a, b, label='unknown'):
        if not isinstance(a, Point) or not isinstance(b, Point):
            raise TypeError("Edge must be initialized with Edge(Point, Point)")

        if label in classifications:
            self.label = label
        else:
            raise TypeError("Invalid edge label given.  Valid labels are " + str(classifications))

        super(Edge, self).__init__(a, b)

    def set_edge_label(self, label):
        if label in classifications:
            self.label = label
        else:
            print("Warning.  Could not set label to " + str(label) + "  Valid labels are " + str(classifications))


classifications = ['unknown', 'footprint', 'vertical', 'eave', 'rake', 'ridge', 'transition', 'hip', 'valley', 'horizontal-interior', 'horizontal-interior-undetermined', 'non-horizontal-interior', 'non-horizontal-interior-undetermined', 'no edge vertex']


__all__ = ("Edge", )
