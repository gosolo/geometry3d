# -*- coding: utf-8 -*-
"""Point Module"""
from ..utils.util import unify_types
import math
from ..utils.logger import get_main_logger
from ..utils.constant import get_sig_figures,get_eps
from ..utils.vector import Vector
from .point import Point
from .plane import Plane
from .polygon import Polygon

#from Geometry3D.Geometry3D import origin, intersection, distance, get_segment_from_point_list, get_projection_length
from ..calc.distance import distance
from ..calc.aux_calc import get_segment_from_point_list, get_projection_length

import copy
from itertools import combinations

def projected_intersection(a, b):
    """
    Returns the line segment on the line of intersection of the 2 planes of 2 convex polygons created by projecting all the 
      polygon vertices onto the line of intersection.
    **Input:**
    -- a: convex polygon
    -- b: convex polygon
    **Output:**
    -- line segment
    """
    if not isinstance(a,Polygon) or not isinstance(a, Polygon):
        raise TypeError('Planar intersection requires two Polygon input parameters')
    if a.plane.n.parallel(b.plane.n):
        return None
    
    ridge_line = Plane.intersection(a.plane, b.plane)
    
    # Get all the projected points onto the line of intersection
    projected_points_a = set()
    for vertex in a.points:
        projected_points_a.add(Point(ridge_line.sv + get_projection_length(Vector(Point(ridge_line.sv),vertex),ridge_line.dv) * ridge_line.dv.normalized()))

    projected_points_b = set()
    for vertex in b.points:
        projected_points_b.add(Point(ridge_line.sv + get_projection_length(Vector(Point(ridge_line.sv),vertex),ridge_line.dv) * ridge_line.dv.normalized()))
    
    segment_a = get_segment_from_point_list(list(projected_points_a))
    segment_b = get_segment_from_point_list(list(projected_points_b))

    return segment_a if segment_a.length() <= segment_b.length() else segment_b


class Vertex(Point):
    """
    Vertex of a polyhedron 

    Formed from the intersection of 3+ planes defined by 3+ polygons
    """
    def __init__(self, *args):

        # Takes 3 Polygons as input
        if len(args) != 3:
            raise TypeError("Vertex() requires exactly three Polygon arguments")
        if (not isinstance(args[0], Polygon) or 
            not isinstance(args[1], Polygon) or 
            not isinstance(args[2], Polygon)):
            raise TypeError("Vertex() takes three Polygon arguments")

        self._polygons = copy.deepcopy(args)
        self._polygons = list(self._polygons)

        # Intersect 3 planes to get Point
        if self._polygons[0].plane.n.parallel(self._polygons[1].plane.n):
            raise ValueError("Cannot create vertex.  Two of the polygons' planes are parallel.") # parallel planes
        intersecting_line = Plane.intersection(self._polygons[0].plane, self._polygons[1].plane)
        if intersecting_line.parallel(self._polygons[2].plane):
            raise ValueError("Cannot create vertex.  The polygons' planes are either parallel, intersect at the same line, or form a prism.") # line parallel to 3rd plane
        point = Plane.intersection(intersecting_line, self._polygons[2].plane)

        super(Vertex, self).__init__(point.x, point.y, point.z)

        # Intersect 2 polygons to get Segments
        self._segments = []
        for two_of_three_polygons in combinations(self._polygons, 2):
            projected_segment = projected_intersection(two_of_three_polygons[0], two_of_three_polygons[1])
            if projected_segment is not None:
                self._segments.append(projected_segment)

    def segments(self):
        for segment in self._segments:
            yield segment

    def polygons(self):
        for polygon in self._polygons:
            yield polygon

    def distance(self):
        """
        Arbitrary distance metric to help measure vertex quality
        Currently defined as the sum of distances from point to all 3+ segments + sum of distances from point to all 3+ polygons
          A "good" vertex will be close to all 3 segments and all 3 polygons
          A "bad" vertex will be formed from a polygon in a distant area of the roof
          In between "good" and "bad" will likely be vertices defined where data is missing; a tree occludes a corner area
        """
        sumDistance = 0
        for segment in self._segments:
            sumDistance += distance(self, segment)

        for poly in self._polygons:
            sumDistance += distance(self, poly)

        return sumDistance

    def merge(self, otherVertex):
        if self != otherVertex:
            return # Warn?  Cannot merge

        for segment in otherVertex.segments():
            if segment not in self._segments:
                self._segments.append(segment)

        for polygon in otherVertex.polygons():
            if polygon not in self._polygons:
                self._polygons.append(polygon)


__all__ = ("Vertex")